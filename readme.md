# E-commerce project

This is anying about  E-commerce project

## Install instruction
0. make sure you have docker in your machine, and port 3000-3003 are free.
1. clone ecom-runner project "git clone https://gitlab.com/e-commerce33/runner.git"
2. run "docker-compose build"
3. run "docker-compose up -d"

## Services definition
- Authentication service run on http://127.0.0.1:3000
- User service run on http://127.0.0.1:3001
- produce service run on http://127.0.0.1:3002
- order service run on http://127.0.0.1:3003

## HTTP authorization header
This project uses Bearer Authentication to identity user.
You will get jwtToken from Authentication service by register or login which get accessToken key.

## API swagger
You can explore REST API of the Swagger UI using the following URL:
```bash
http://{HOST}:{PORT}/api-docs/
```
## git resource
Visit all services in this project on https://gitlab.com/e-commerce33